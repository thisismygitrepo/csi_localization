
import numpy as np
import matplotlib.pyplot as plt
import crocodile.toolbox as tb
import tensorflow as tf
from utils import complex as cx, data_reader as dr


def get_autoencoder():
    model = tf.keras.Sequential([cx.ComplexDense(50), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(20), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(10), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(5), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(10), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(20), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(50), cx.Act(cx.CActivaions.crelu),
                                 cx.ComplexDense(100),
                                 ])
    return model


def main():
    d = dr.DataReader()
    return d


if __name__ == '__main__':
    pass
