
import numpy as np
import matplotlib.pyplot as plt
import crocodile.toolbox as tb


class DataReader:
    def __init__(self, topology=1):
        self.root = tb.P.home().joinpath("data/ultra_dense")
        self.topology = ["DIS_lab_LoS", "ULA_lab_LoS", "URA_lab_LoS", "URA_lab_nLoS"][topology]
        self.data_path = self.root / self.topology
        self.data = self.read_topology(self.data_path)
        self.quad = None

    @staticmethod
    def read_topology(path):
        ap = (path / "antenna_positions.npy").readit()
        up = (path / "user_positions.npy").readit()
        samples = (path / "samples").search("*.npy")
        result = tb.Struct(ap=ap, up=up, samples=samples)
        return result

    def plot_array_and_positions(self, save=False):
        tb.FigureManager.activate_latex()
        fig, ax = plt.subplots(figsize=(8, 8))
        ax.scatter(*self.data.ap[:, :2].T, label="Antenna Position", s=4)
        ax.scatter(*self.data.up[:, :2].T, label="User Position", s=0.5)
        tb.FigureManager.grid(ax)
        ax.set_title("User and Array")
        ax.set_xlabel("X in cm")
        ax.set_ylabel("Y in cm")
        ax.legend()
        if save:
            fig.savefig(tb.tmp() / "setup.png", dpi=200)
            print(f'Figure Saved @ {tb.tmp() / "setup.png"}')

    def get_generator(self):
        """Data is not read because of its size, instead a lazy generator is made here:"""
        def generator():
            for file, pos in zip(self.data.samples, self.data.up[:, :2]):
                csi = file.readit()
                yield csi, pos
        return generator

    def get_tf_pipeline(self):
        import tensorflow as tf
        dataset = tf.data.Dataset.from_generator(self.get_generator(),
                                                 output_signature=(
                                                     tf.TensorSpec(shape=(64, 100), dtype=tf.complex64),
                                                     tf.TensorSpec(shape=(2,), dtype=tf.float32)))
        return dataset

    def plot_path(self, dat, quad=True):
        fig, ax = plt.figure(figsize=(8, 8))
        if quad:
            ax.scatter(self.quad[:, :, 0].flatten(), self.quad[:, :, 1].flatten(), s=0.1)
        ax.plot(dat[:, 0], dat[:, 1], color="red")

    def get_data_from_path(self, path):
        """
        :param path: set of indices.
        :return: The corresponding XY locations, and the corresponding CSI matrices.
        """
        csi_path = self.quad[path[:, 0], path[:, 1]]
        return tb.Struct(path_index=path,
                         path_loc=csi_path,
                         csi=self.data.samples[csi_path[..., -1]].apply(lambda x: x.readit()).to_numpy())

    def get_quadrant(self, x="right", y="top"):
        """

        :param x: "right" or "left"
        :param y: "top" or "bottom"
        :return: One quadrant of the data. This is useful for creation of a random walk.
        """
        left = self.data.up[:, 0] < 0
        right = self.data.up[:, 0] > 0
        top = self.data.up[:, 1] > 2500
        bottom = self.data.up[:, 1] < 2500
        quad_dict = tb.Struct(right=right, top=top, left=left, bottom=bottom)
        indices = quad_dict[x] & quad_dict[y]
        data = self.data.up[indices]
        file_idx = np.arange(self.data.up.shape[0])[indices]
        data = np.concatenate([data, file_idx[:, None]], axis=1)
        y_length = np.where(data[:, 1] == data[0, 1])[0].shape[0]
        res = []
        for idx, item in enumerate(np.split(data, y_length, axis=0)):
            if idx % 2 == 0:
                # the data was collected such that when you finish a column, you start the next one from the bottom.
                res.append(item)
            else:
                res.append(np.flip(item, axis=0))
        res = np.array(res)
        self.quad = res
        return res


class RandomWalk:
    """Create random walk within given limits"""
    def __init__(self, x_lim=251, y_lim=251):
        self.x_lim = x_lim
        self.y_lim = y_lim

    def get_random_point(self):
        return np.array([np.random.choice(item) for item in (self.x_lim, self.y_lim)])

    def get_random_start_end(self):
        st = self.get_random_point()
        ed = self.get_random_point()
        return st, ed

    def get_random_path(self, st=None, ed=None, p=0.3, plot=True, min_len=100):
        """

        :param min_len: minimum length of the path
        :param st:
        :param ed:
        :param p: move to goal 70% of the time
        :param plot:
        :return:
        """
        if st is None:
            st, ed = self.get_random_start_end()

        path = [np.copy(st)]
        nxt = np.copy(st)

        while all(nxt != ed):

            # 1: pick a random axis
            coord = np.random.choice(["x", "y"])
            index = {"x": 0, "y": 1}[coord]

            # 2: decide on what to do in it:
            if np.random.rand() > (1 - p):
                change = ed[index] - nxt[index]  # points in the direction of the end
                change = np.sign(change)
            else:  # move randomly
                change = np.random.choice([-1, 0, 1])

            # 3: bounds checking
            if nxt[index] == 0 and change < 0:
                change = 0
            if nxt[index] == [self.x_lim, self.y_lim][index] and change > 0:
                change = 0

            # 4: apply change
            nxt[index] += change
            path.append(np.copy(nxt))

        path = np.array(path)

        if len(path) < min_len:
            print(f"Not enough length, trying again ...")
            return self.get_random_path(st, ed, p, plot, min_len)
        else:
            if plot:
                plt.plot(*path.T)
            return path


def main():
    d = DataReader()
    d.get_quadrant()
    rw = RandomWalk()
    bob = d.get_data_from_path(rw.get_random_path())
    eve = d.get_data_from_path(rw.get_random_path())


if __name__ == '__main__':
    # main()
    pass
