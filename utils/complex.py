
"""
Library of complex blocks of deep learning
"""

import tensorflow as tf
from tensorflow.keras.layers import Activation as Act
import tensorflow.keras.activations as act


class ComplexDense(tf.keras.layers.Layer):
    def __init__(self, units=1, input_dim=None, use_bias=True):
        """Mapping from M ComplexInput to N ComplexOutput.
        Orindary layer ==> 2 * M RealInput and 2 * N RealOutput ==> Total Real Weights = 2 * M * 2 * N + N * 2
        Complex layer ==> Total Real Weights = M * 2 * N + N * 2. (See w_re and w_im attributes)
        Thus, savings occur with the ability to handle complex input. Both cases require to double the output.
        That said, the savings come at the cost of reducing expressive power of the network. In particular,
        Google this: matrix equivalent of complex multiplication. However, given that the input features are not
        independent but rather form a holomorphic function, this could be a reasonable choice.

        :param units: complex nuerons
        :param input_dim: complex input dimensions
        :param use_bias: binary flag
        """
        super(ComplexDense, self).__init__()
        self.units = units
        self.input_dim = input_dim
        self.use_bias = use_bias
        self.w_re = self.w_im = self.b_re = self.b_im = None
        if self.input_dim:
            self.build((None, input_dim))

    def build(self, ip_shape):
        self.w_re = self.add_weight(shape=(ip_shape[-1], self.units), initializer="random_normal", trainable=True)
        self.w_im = self.add_weight(shape=(ip_shape[-1], self.units), initializer="random_normal", trainable=True)
        if self.use_bias:
            b_init = tf.zeros_initializer()
            self.b_re = tf.Variable(initial_value=b_init(shape=(self.units,), dtype="float32"), trainable=True)
            self.b_im = tf.Variable(initial_value=b_init(shape=(self.units,), dtype="float32"), trainable=True)

    def call(self, inputs):
        re = tf.math.real(inputs)
        im = tf.math.imag(inputs)
        return tf.complex(tf.matmul(re, self.w_re) - tf.matmul(im, self.w_im) + self.b_re,
                          tf.matmul(im, self.w_im) + tf.matmul(re, self.w_im) + self.b_im)

    def __call__(self, *args, **kwargs):
        return super(ComplexDense, self).__call__(*args, **kwargs)


class CActivaions:
    @staticmethod
    def crelu(x):
        return tf.complex(act.relu(tf.math.real(x)),
                          act.relu(tf.math.real(x)))


def main():
    batch_size = 10
    ip_size = 1
    data = tf.complex(tf.random.normal((batch_size, ip_size), dtype=tf.float32),
                      tf.random.normal((batch_size, ip_size), dtype=tf.float32))
    cd = ComplexDense()
    q = cd(data)
    return q


if __name__ == '__main__':
    pass
